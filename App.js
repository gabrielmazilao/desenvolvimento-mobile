import { StyleSheet, View, ActivityIndicator } from 'react-native';
import * as React from 'react';
import { HelperText, TextInput, Button } from 'react-native-paper';

export default function App() {
  const [senha, setSenha] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [senhaVisivel, setSenhaVisivel] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);

  const onChangeSenha = senha => setSenha(senha);
  const onChangeEmail = email => setEmail(email);

  const hasErrors = () => {
    return !email.includes('@');
  };

  const toggleSenhaVisivel = () => {
    setSenhaVisivel(!senhaVisivel);
  };

  const handleLogin = () => {
    setIsLoading(true);

    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  };

  return (
    <View style={styles.container}>
      <TextInput label="Email" value={email} onChangeText={onChangeEmail} right={<TextInput.Icon icon='email'/>} />
      <HelperText type="error" visible={hasErrors()}>
        Email address is invalid!
      </HelperText>
      <TextInput
        label="Senha"
        value={senha}
        onChangeText={onChangeSenha}
        secureTextEntry={!senhaVisivel}
        right={
          <TextInput.Icon
            name={senhaVisivel ? 'eye' : 'eye-off'}
            onPress={() => setSenhaVisivel(!senhaVisivel)}
            icon="eye"
          />
        }
      />
      <Button
        style={[styles.button, isLoading && styles.buttonDisabled]}
        icon="login"
        mode="contained"
        onPress={handleLogin}
        disabled={isLoading}
      >
        {isLoading ? (
          <ActivityIndicator color="white" size="small" />
        ) : (
          'Entrar'
        )}
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",
    margin: 15,
    borderRadius: 20,
    height: "40%",
    shadowColor: "black",
    shadowRadius: 10,
  },
  button: {
    marginTop: 30,
    backgroundColor: 'purple',
  },
  buttonDisabled: {
    backgroundColor: 'purple',
    opacity: 0.6,
  },
});
